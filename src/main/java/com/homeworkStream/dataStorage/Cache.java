package com.homeworkStream.dataStorage;

import com.homeworkStream.model.Student;
import java.util.List;

public class Cache {

    Student student1 = new Student(1, "name1", "lname1", 1989, "Kiev", "+38097", "biology", "first", "group1");
    Student student2 = new Student(2, "name2", "lname2", 1999, "Kharkiv", "+38097", "it", "first", "group1");
    Student student3 = new Student(3, "name3", "lname3", 1992, "Dnepr", "+38097", "biology", "second", "group2");
    Student student4 = new Student(4, "name4", "lname4", 1994, "Kiev", "+38097", "law", "first", "group1");
    Student student5 = new Student(5, "name5", "lname5", 1992, "Kharkiv", "+38097", "it", "second", "group2");
    Student student6 = new Student(6, "name6", "lname6", 1993, "Kiev", "+38097", "medicine", "third", "group3");
    Student student7 = new Student(7, "name7", "lname7", 1993, "Dnepr", "+38097", "law", "fourth", "group4");
    Student student8 = new Student(8, "name8", "lname8", 1991, "Kiyv", "+38097", "it", "third", "group3");
    Student student9 = new Student(9, "name9", "lname9", 1988, "Dnepr", "+38097", "medicine", "third", "group3");

    List<Student> listOfStudents = List.of(student1, student2, student3, student4, student5, student6, student7, student8, student9);

    public List<Student> getListOfStudents () {
        return this.listOfStudents;
    }

}
